#!/usr/bin/perl

use strict;
use WWW::Mechanize;
use WWW::Mechanize::TreeBuilder;
use HTML::TreeBuilder;

use HTML::TableExtract;
use POSIX qw(strftime); 

my $mech = WWW::Mechanize->new(cookie_jar => {});
WWW::Mechanize::TreeBuilder->meta->apply($mech);

my $eurKupovni;
my $eurProdajni;
my $usdKupovni;
my $usdProdajni;

$mech->get('https://SOME-WEB-SITE/WITH/UL-DATA');

my $div = $mech->look_down('_tag' => 'div', 'class' => 'WITH ul DATA');

my $html_tree = HTML::TreeBuilder->new;

$html_tree->parse($div->as_HTML());

my @li_tags = $html_tree->look_down("_tag", "li");

for (my $i = 0; $i < $#li_tags; $i++) {
	# Each li has following structure:
	#
	# <h3>Title</h3>
	# <p>
	# Data of interest
	# </p>
	#
	my $li_tree = HTML::TreeBuilder->new;
	$li_tree->parse($li_tags[$i]->as_HTML());
	my $curTitle = $li_tree->look_down("_tag", "h3");
	my $pTag = $li_tree->look_down("_tag", "p");

	# Take Data of Interest
	$pTag->as_text =~ m/Kupovni:\s*([0-9]+[,|\.][0-9]+)/;
	my $curKupovni = $1;
	$curKupovni =~ s/,/\./;
	$pTag->as_text =~ m/Prodajni:\s*([0-9]+[,|\.][0-9]+)/;
	my $curProdajni = $1;
	$curProdajni =~ s/,/\./;

	if ($curTitle->as_text =~ m/EUR 978/) {
		$eurKupovni = $curKupovni;
		$eurProdajni = $curProdajni;
	}
	if ($curTitle->as_text =~ m/USD 840/) {
		$usdKupovni = $curKupovni;
		$usdProdajni = $curProdajni;
	}
}